
import { Component, OnInit} from '@angular/core';

import { FormArray, FormBuilder, FormGroup } from '@angular/forms';

import {Observable,of} from 'rxjs';

import {debounceTime, map, startWith} from 'rxjs/operators';

import { TableService } from './table.service';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  tableForm :FormGroup;
  filteredOptionsCity: Observable<string[]>;
  filteredOptinsCountry: Observable<string[]>;
  allDetails=[];
  countrys=[];
  cities=[]
  idSelected=''
  formData=''

  constructor(private _fb:FormBuilder,private _countryDetails :TableService) { }

  ngOnInit() {
      this.formData= localStorage.getItem('formdata')

       this.builtForm();
       if(this.formData){
        this.patchFormData(JSON.parse(this.formData));
      }
       this.getCountryDetails();
  }

  builtForm(){
    this.tableForm = this._fb.group({
      rows:this._fb.array([])
    })
   this.addNewRow();
  }

  addCol(rowIndex,colIndex){

    let row = this.tableForm.get('rows') as FormArray;
    let col= row.controls[rowIndex].get('coln') as FormArray;
    let FormcoltrolName ='row'+rowIndex+'col'+colIndex+ Math.floor(Math.random() * 100);
    col.insert(colIndex,this.addNewCol(FormcoltrolName));
    this.addAdditionalCols(col.controls.length,colIndex)
  }

  addNewRow(){
    let row = this.tableForm.get('rows') as FormArray;
      row.push(this.addRow())
      this.addCol(0,0);
      this.addCol(0,1);
  }

  addRowWithIndex(rowIndex){
    let row = this.tableForm.get('rows') as FormArray;
    row.insert(rowIndex,this.addRow());
    let col= row.controls[rowIndex].get('coln') as FormArray;
    let defaultcol= row.controls[0].get('coln') as FormArray;
    defaultcol.controls.forEach((item ,index)=>{
      let FormcoltrolName ='row'+rowIndex+'col'+index+ Math.floor(Math.random() * 100);
      col.insert(index,this.addNewCol(FormcoltrolName));
    })
  }

  addRow(){
    return this._fb.group({
      coln:this._fb.array([])
    })
  }

  addNewCol(colIndexValue){
    let colIndex={
      [colIndexValue]:''
    }
    return this._fb.group(
     colIndex
    )
  }

  addAdditionalCols(maxColLength,colIndex?){
    let row = this.tableForm.get('rows') as FormArray;
    row.controls.forEach((item,index )=>{
     let col=item.get('coln') as FormArray;
     if(col.controls.length<maxColLength){
      let colLength =col.controls.length;
      let colIndexValue =Number(colIndex)
         while(maxColLength!=colLength){
          let FormcoltrolName ='row'+Number(index)+'col'+ Number(colLength-1)+ Math.floor(Math.random() * 100);
          col.insert(colIndexValue,this.addNewCol(FormcoltrolName));
          colIndexValue++
          colLength++
         }
     }
    })
  }

  returnFormcontrolName(col){
  let formControlName =  Object.keys(  col.value );
  return formControlName

  }

  private _filterCity(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.cities.filter(option => option?option.toLowerCase().includes(filterValue):false);
  }

  private _filterCountry(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.countrys.filter(option => option.toLowerCase().includes(filterValue));
  }

  submit(){
    let formdata = JSON.stringify(this.tableForm.value);
    localStorage.setItem('formdata',formdata)
  }

  inputValueChangeCountry(form : FormGroup){
    let key = Object.keys(form.value)
     this.filteredOptinsCountry = form.get(key).valueChanges.pipe(
      debounceTime(10),
      startWith(''),
      map(value => this._filterCountry(value))
    );
  }

  inputValueChangeCity(form : FormGroup){
    let key = Object.keys(form.value)
     this.filteredOptionsCity = form.get(key).valueChanges.pipe(
      startWith(''),
      map(value => this._filterCity(value))
    );
  }

  getCountryDetails(){
    let countryAndCityData=of(this._countryDetails.getCountryDetails());
    countryAndCityData.subscribe(data=>{
      this.allDetails = data;
      this.cities = this.allDetails.map(item =>item.city);
      this.countrys= this.allDetails.map(item =>item.country);
    })
  }

  onChangeCountry(i){
    let row = this.tableForm.get('rows') as FormArray;
    let col= row.controls[i].get('coln') as FormArray;
    setTimeout(() => {
      let countryValue = Object.values(col.controls[0].value)
      let cityKey= Object.keys(col.controls[1].value)
      let cityName = this.allDetails.filter(allData => allData.country ==countryValue)
      col.controls[1].get(cityKey).setValue(cityName[0].city)
    }, 1000);
  }

  onChangeCity(i){
    let row = this.tableForm.get('rows') as FormArray;
    let col= row.controls[i].get('coln') as FormArray;
    setTimeout(() => {
      let cityValue = Object.values(col.controls[1].value)
      let countryKey= Object.keys(col.controls[0].value)
      let countryName = this.allDetails.filter(allData => allData.city ==cityValue)
      col.controls[0].get(countryKey).setValue(countryName[0].country)
    }, 1000);
  }

  deleterow(index){
    let row = this.tableForm.get('rows') as FormArray;
    row.removeAt(index)
  }

  openMenu(id) {
    this.idSelected=id;
  }

  patchFormData(data){
     for (const key in data) {
       if (Object.prototype.hasOwnProperty.call(data, key)) {
         if(key=='rows'){
          const rows = data[key];
          rows.forEach((item,index) => {
            if(index>0){
              this.addRowWithIndex(index);
            }
          });
         }
       }
     }
    this.addAdditionalCols(data['rows'][0].coln.length);
    this.patchFormItems(data)

  }

  patchFormItems(data){

    let rowsData=[]
    for (const key in data) {
      if (Object.prototype.hasOwnProperty.call(data, key)) {
        if(key=='rows'){
         const rows = data[key];
         rows.forEach((item,index) => {
           rowsData.push(item.coln)
           })
        }
      }
    }

    rowsData.forEach((items,rowindex)=>{
      let row = this.tableForm.get('rows') as FormArray;
      let col = row.controls[rowindex].get('coln') as FormArray;
      items.forEach((item,colindex) => {
      let colFormControlName=  Object.keys(col.controls[colindex].value)[0];
      let colValue =Object.values(item)[0];
      col.controls[colindex].get(colFormControlName).setValue(colValue)
      });
    })

  }

}
