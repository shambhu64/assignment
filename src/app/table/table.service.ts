import {countryDetails} from './../constant.class';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TableService {
  constructor(private _http:HttpClient) {


   }

  //  getCountryDetails():Observable<any>{
  //    return this._http.get(URL)
  //  }

   getCountryDetails(){
     return countryDetails
   }
}
